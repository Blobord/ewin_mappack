
local mapids = {}

-- ttt
mapids["ttt_minecraft_b5"] = "159321088"
mapids["ttt_lost_temple_v2"] = "106527577"
mapids["ttt_whitehouse_b2_d11"] = "285878671"
mapids["ttt_clocktown_v1"] = "220740097"
mapids["ttt_67thway_rework_b1"] = "105195237"
mapids["ttt_lttp_kakariko_a4"] = "118937144"
mapids["ttt_island_2013"] = "183797802"
mapids["ttt_rooftops_2016_v1"] = "534491717"
mapids["ttt_clue_se"] = "281454209"
mapids["ttt_roy_the_ship"] = "108040571"
mapids["ttt_pokemon_v2"] = "396645991"
mapids["ttt_outset_island"] = "761923626"
mapids["ttt_crummycradle_a4"] = "264864627"
mapids["ttt_dolls_day_fps_upd2"] = "911758202"
mapids["ttt_arctic_complex_b3"] = "489548403"

mapids["ttt_concentration_b2"] = "228509308"
mapids["ttt_helms_deep"] = "438260177"
mapids["ttt_moonbase_redux_mg_b2"] = "345094519"
mapids["ttt_mttresort"] = "719634946"
mapids["ttt_pacman_v1"] = "379662785"
mapids["ttt_pcworld95_final"] = "624717817"
mapids["ttt_peachs_castle"] = "1092110486"
mapids["ttt_canyon_a4"] = "224282233"
mapids["ttt_innocentmotel_v1"] = "805127639"
mapids["ttt_waterworld"] = "157420728"

-- deathrun
mapids["deathrun_iceworld_v4"] = "308695657"
mapids["deathrun_undertale"] = "732890042"
mapids["deathrun_simpons_finalfix"] = "215951766"
mapids["deathrun_portal"] = "1460475406"
mapids["deathrun_windows_xp"] = "1269262622"
mapids["deathrun_helix_b8"] = "215905830"
mapids["deathrun_goldfever"] = "216585568"
mapids["deathrun_sonic_fix"] = "531427338"
mapids["deathrun_minecraft_b3_fix2"] = "1078745990"
mapids["deathrun_mario_v1"] = "423865896"
mapids["deathrun_marioworld_finalob"] = "219378433"
mapids["dr_minecraft"] = "104682188"

-- rp
mapids["rp_florida_v2"] = {"1526892946", "1526875975", "1526881546", "1526887816"}
mapids["rp_bangclaw"] = "111863064"
mapids["rp_downtown_tits_v1"] = "1186766769"
-- ph
mapids["ph_restaurant_2019"] = "1627575684"

mapids["ph_youturd"] = "1110852560"
mapids["ph_lttp_kakariko_b2"] = "282350293"
mapids["ph_underwataaa"] = "224680268"
mapids["ph_starship"] = "271354698"
mapids["ph_minecraft_awakening_a3"] = "339518667"
mapids["ph_islandhouse"] = "237152021"
mapids["ph_crashhouse"] = "205719925"


mapids["ph_supermarket"] = "1432624595"
mapids["ph_bikinibottom"] = "1123262520"
mapids["ph_resortmadness"] = "1389282477"
mapids["ph_oceanwaves"] = "1654704542"


mapids["ph_villa"] = "1397337244"
mapids["ph_westerncity"] = "1738480835"
mapids["ph_anonhide"] = "741741270"
mapids["ph_uncivil_hunt"] = "1456916318"

-- md
mapids["md_clue"] = "217808514"
mapids["mu_greenwood"] = "286757719"
mapids["md_ironsky_a6"] = "188652909"
mapids["md_smallotown_v2"] = "541552387"
mapids["md_russiankvartal_rf"] = "1412247651"
mapids["mu_gmart_v1"] = "403290326"
mapids["md_depot"] = "1144688934"
mapids["md_tudor_hall"] = "415683233"
mapids["md_horrorforest"] = "963157394"
mapids["md_whitehills_b2"] = "195685880"
mapids["md_shiningmaze"] = "775750653"
mapids["mu_shopstreet"] = "926247431"
mapids["mu_office"] = "275775110"
mapids["mu_forest"] = "492480279"
mapids["mu_factory"] = "288114460"
mapids["mu_bistro"] = "471052889"
mapids["mu_parking"] = "508192233"

-- zs
mapids["zs_snowdin_town_v1b"] = "576159801"
mapids["zs_castle_keep_snowy"] = "340180450"
mapids["zs_closure"] = "1569126369"
mapids["zs_snowedin"] = "326578232"
mapids["zs_krusty_krab_large_v5"] = "645191518"
mapids["zs_obj_bluevelvet_v4"] = "210959454"
mapids["zs_obj_rescape_v13"] = "617404934"
mapids["zs_obj_vertigo_v0"] = "110985664"
mapids["ze_ffvi_mako_reactor_v5_3"] = "307755108"
mapids["zs_obj_gauntlet_v3"] = "129036224"
mapids["zs_abandoned_mall_v9"] = "1084464522"
mapids["zs_blitzkrieg_v6"] = "297974435"
mapids["zs_deus_ex_unatco_v3"] = ""
mapids["zs_insurance"] = "181428525"
mapids["zs_jail_v1"] = "240000774"
mapids["zs_obj_6_nights_v8"] = "777948312"	
mapids["zs_obj_827_to_phoenix_v8"] = "586931772"
mapids["zs_obj_dump_v14"] = "112595416"
mapids["zs_onett_v6"] = "620613205"
mapids["zs_swimming_pool_v2"] = "154887708"
mapids["zs_obj_minecave"] = "345214265"
mapids["zs_obj_lambdacore_v21"] = "644445805"
mapids["zs_obj_escapecity17_v2"] = "755129901"
mapids["zs_yzarc_station"] = "1407923339"
mapids["zs_minecraft_oasis_v1"] = "530017496"
mapids["zs_obj_vertigo_v25"] = "160096524"
mapids["zs_the_thing_v3a"] = "1422872361"

mapids["zs_minecraft_ricetown_b2"] = "669899053"
mapids["zs_toytown_v8"] = "1491226330"
mapids["zs_storm_v1"] = "436114991"
mapids["zs_obj_mental_hospital_v6"] = "173708148"
mapids["zs_the_pub_final7"] = "542535318"
mapids["zs_imashouse"] = "183229670"
mapids["zs_abstraction_night"] = "663750710"
mapids["zs_obj_upward_v23"] = "644470714"
mapids["zs_snow_v1"] = "269920456"
mapids["zs_closure"] = "1569126369"
mapids["zs_eris_v3"] = "1432645176"
mapids["zs_hamlet_town_v1a"] = "1235659201"
mapids["zs_minecraft_hsisland_b1"] = "1278934423"
mapids["zs_the_citadel"] = "185200173"
mapids["zs_deus_ex_hongkong_v3"] = "648571820"
mapids["zs_pantsfactory_v1"] = "214194354"

mapids["zs_city_locker"] = "723077312"

mapids["zs_houseparty"] = "315858994"
mapids["zs_amsterville_v5"] = "702586059"
mapids["zs_barren"] = "181159944"
mapids["zs_plaza_v9"] = "767887849"
mapids["zs_hotline_miami_v3"] = "748019993"
mapids["zs_station_mall_v2"] = "1514423378"
mapids["zs_castle_mortis"] = "833071229"
mapids["zs_mesa_cliffside_v1"] = "338632001"
mapids["zs_obj_reactor_15_v2"] = "272307480"

mapids["zs_last_day_z"] = "319254405"
mapids["zs_ancient_sand_v3"] = "341489111"
mapids["zs_darkside"] = "325848556"
mapids["zs_snowy_castle"] = "320213706"
mapids["zs_the_last_day_z"] = "319254405"

mapids["zs_slugde"] = "323893414"
mapids["zs_18_v1a"] = "1625359200"
mapids["zs_snowedin"] = "326578232"
mapids["zs_cavecomplex_v2"] = "278418169"
mapids["zs_vault_106_v7"] = "110983920"
mapids["zs_clav_massive"] = "172051398"
mapids["zs_woodhouse_rain"] = "269921567"
mapids["zs_festive_villa_v1"] = "674795014"
mapids["zs_obj_filth_v17"] = "854686618"
mapids["zs_obj_station"] = "222854611"
mapids["zs_clav_chaossystem"] = "172048103"
mapids["zs_obj_tantibus_v8"] = "1286190528"
mapids["zs_riften_v1"] = "692601530"
mapids["zs_obj_station"] = "222854611"
mapids["zs_obj_enervation_v16"] = "812163491"
mapids["zs_obj_mine_v2"] = "825038140"
mapids["zs_subterranean_v1"] = "466332352"
mapids["zs_bunker_fixed_v2"] = "180897118"
mapids["zs_subway_v9"] = "960750746"

mapids["zs_obj_event_horizon_v4"] = "962455362"
mapids["zs_obj_blackout_v4a"] = "1119449157"

mapids["zs_nuketown__x_town"] = "1758295793"

mapids["zs_pub_v5"] = nil
mapids["zs_ravenholm_v7"] = nil
mapids["zs_obj_rampage_v7"] = nil
mapids["zs_lostfarm_v9"] = nil
mapids["zs_nastierhouse_final_v2"] = nil

local oadd = resource.OldAddWorkshop or resource.AddWorkshop
local add = resource.AddWorkshop


local curmapid = mapids[string.lower(game.GetMap())]

if(not curmapid or not isstring(curmapid)) then
	hook.Add("PlayerSpawn","k", function(p)
		p:ChatPrint("DOWNLOADS ERROR - NO MAPID FOR MAP: " .. game.GetMap())
	end)
	return
end


if(istable(curmapid)) then
	for k,v in pairs(curmapid) do
		oadd(v)
	end
	return
end

oadd(curmapid)
